
import { useEffect, useState } from "react";
import ReactFlagsSelect from "react-flags-select";

function App() {

  const [language, setLanguage] = useState("GB");
  const [content, setContent] = useState({});
  const [projectDetails, setProjectDetails] = useState({});

  const translations = {
    GB: {
      menu: {
        home: 'Home',
        services: 'Services',
        portfolio: 'Projects',
        team: 'Team',
        contact: 'Contact Us'
      },
      banner: {
        subtitle: 'Welcolme at Pi357',
        title: 'Where every problem has a solution',
        text: 'We provide digital solutions to enhance investment in Mobility & We make it Riskless and Sustainable.',
        button: 'Need Help ?'
      },
      services: {
        subtitle: 'Our Services',
        title: 'What do we <em>provide</em>',
        service_title1: 'Care',
        service_text1: 'With our large network of Mechanics & Partners, we provide the quickest Care and assistance.',
        service_title2: 'Insurance',
        service_text2: 'In partnership with the largest insurance companies in the country, we provide activity insurance.',
        service_title3: 'Cashless',
        service_text3: 'We provide a secure Cashless platform for the payment through our platform via all the existing Payment Method.',
        service_title4: 'Ads',
        service_text4: 'We help our vehicle owner or Fleet Manager partners to have substantial income.',
        service_title5: 'Fuel',
        service_text5: 'With our partnership with Fuel Station, All our partners have Fuel at the Lowest pricing of the market.',
        service_title6: 'Leasing',
        service_text6: 'Throughout our Network of Car Manufacturer , We help our partners, to obtain easily a Leasing.',
      },
      portfolio: {
        subtitle: "Our Portfolio",
        title: "Our Recents <em>Projects</em>",
        project_title1: "Taximeter",
        project_title2: "Bus",
        project_title3: "Cargo",
        details: {
          taximeter: "With PiTaxi, we are enabled to offer\
          <ul>\
          <li>A full occupancy offline or online</li>\
          <li>Cashless payments</li>\
          <li>A realtime management for both the revenue and the mechanicals specs</li>\
          </ul>",
          bus: "With PiBus, we are enabled to offer\
          <ul>\
          <li>Dynamics stops</li>\
          <li>Realtime location & availability of seat of the bus</li>\
          <li>A ticketing system with an interoperability with all types of Sharing Transportation types</li>\
          </ul>",
          cargo: "With PiCargo, we are enabled to offer\
          <ul>\
          <li>An automated Geofencing System with status in every steps of the trips</li>\
          <li>A fuel, Goods weight , Tyres states and dumping automatically during the trips</li>\
          <li>A dashboard for the overall monitoring</li>\
          </ul>",
        }
      },
      team: {
        subtitle: "Our Team",
        title: "A <em>dynamic</em> team",
        member_job1: "Partner & Chief Executive Officer",
        member_job2: "Partner & Chief Financial Officer",
        member_job3: "Partner & Chief Growth Officer",
      },
      contact: {
        subtitle: "Contact Us",
        title: "Stay in <em>touch</em> team",
        placeholder_name: "Full Name",
        placeholder_email: "Email",
        placeholder_phone: "Phone",
        placeholder_message: "Message",
        button: "Send"
      },
      footer: {
        copyright: "Copyright © 2023 Pi357 PLC., All Rights Reserved."
      },
    },
    FR: {
      menu: {
        home: 'Accueil',
        services: 'Nos Services',
        portfolio: 'Nos Projets',
        team: 'Notre Equipe',
        contact: 'Contactez-Nous'
      },
      banner: {
        subtitle: 'Bienvenue à Pi357',
        title: 'Là où chaque problème a une solution',
        text: 'Nous fournissons des solutions numériques pour améliorer l\'investissement dans la mobilité et nous le rendons sans risque et durable.',
        button: 'Besoin d\'aide ?'
      },
      services: {
        subtitle: 'Nos Services',
        title: 'Qu\'est-ce que nous <em>faisons</em> ?',
        service_title1: 'Santé',
        service_text1: 'Grâce à notre vaste réseau de mécaniciens et de partenaires, nous fournissons les soins et l\'assistance les plus rapides.',
        service_title2: 'Assurance',
        service_text2: 'En partenariat avec les plus grandes compagnies d\'assurance du pays, nous proposons une assurance activité.',
        service_title3: 'Paiement en ligne',
        service_text3: 'Nous fournissons une plate-forme sécurisée Cashless pour le paiement via notre plate-forme via tous les modes de paiement existants.',
        service_title4: 'Publicités',
        service_text4: 'Nous aidons notre propriétaire de véhicule ou nos partenaires Fleet Manager à avoir des revenus substantiels.',
        service_title5: 'Carburant',
        service_text5: 'Grâce à notre partenariat avec Fuel Station, tous nos partenaires ont du carburant au prix le plus bas du marché.',
        service_title6: 'Leasing',
        service_text6: 'A travers notre réseau de constructeurs automobiles, nous aidons nos partenaires, à obtenir facilement un Leasing.'
      },
      portfolio: {
        subtitle: "Notre Portfolio",
        title: "Nos <em>Projets</em> Récents",
        project_title1: "Taximeter",
        project_title2: "Bus",
        project_title3: "Cargo",
        details: {
          taximeter: "Avec PiTaxi, Nous pouvons offrir\
          <ul>\
          <li>Une occupation complète hors ligne ou en ligne</li>\
          <li>Paiements sans espèces</li>\
          <li>Une gestion en temps réel des recettes et des spécifications mécaniques</li>\
          </ul>",
          bus: "Avec PiBus, Nous pouvons offrir\
          <ul>\
          <li>Arrêts Dynamiques</li>\
          <li>Localisation en temps réel et disponibilité des sièges du bus</li>\
          <li>Un système de billetterie avec une interopérabilité avec tous les types de transports partagés</li>\
          </ul>",
          cargo: "Avec PiCargo, Nous pouvons offrir\
          <ul>\
          <li>Un système de géorepérage automatisé avec statut à chaque étape des voyages</li>\
          <li>Un rapport sur le carburant, le poids des marchandises, les états de pneus et un vidage automatiquement pendant les trajets</li>\
          <li>Un tableau de bord pour le suivi global</li>\
          </ul>",
        }
      },
      team: {
        subtitle: "Notre Equipe",
        title: "Une équipe <em>dynamique</em>",
        member_job1: "Associé et chef de la direction",
        member_job2: "Associé et directeur financier",
        member_job3: "Associé et directeur de la croissance",
      },
      contact: {
        subtitle: "Contactez-Nous",
        title: "Restons en <em>contact</em> team",
        placeholder_name: "Nom Complet",
        placeholder_email: "Email",
        placeholder_phone: "Téléphone",
        placeholder_message: "Message",
        button: "Envoyer"
      },
      footer: {
        copyright: "Copyright © 2023 Pi357 PLC., Tous droits Réservés."
      },
    }
  }

  const showModal = (event, project) => {
    event.preventDefault()

    if (content.portfolio.details[project]) {
      setProjectDetails(content.portfolio.details[project])

      window.dialog.showModal()
    }
  }

  const closeModal = () => {
    window.dialog.close()
    setProjectDetails({})
  }

  useEffect(() => {
    setContent(translations[language])
  }, [language])

  return (
    <>
      {/*<div id="js-preloader" className="js-preloader">
        <div className="preloader-inner">
          <span className="dot"></span>
          <div className="dots">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
  </div>*/}

      <div className="pre-header">
        <div className="container">
          <div className="row">
            <div className="col-lg-8 col-sm-8 col-7">
              <ul className="info">
                <li><a href="mailto:info@pi357.com"><i className="fa fa-envelope"></i>info@pi357.com</a></li>
                <li><a href="tel:+2250749400000"><i className="fa fa-phone"></i>+225 07 49 40 00 00</a></li>
                <li style={{ width: '60px' }}>
                  <ReactFlagsSelect showOptionLabel={false} showSelectedLabel={false} selected={language} onSelect={code => setLanguage(code)} countries={["GB", "FR"]} />
                </li>
              </ul>
            </div>
            <div className="social-media-container col-lg-4 col-sm-4 col-5">
              <ul className="social-media">
                <li><a href="https://www.linkedin.com/company/pi357-plc/"><i className="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <header className="header-area header-sticky wow slideInDown" data-wow-duration="0.75s" data-wow-delay="0s">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <nav className="main-nav">
                <a href="/" className="logo">
                  <img style={{ width: '20px' }} src="/assets/logo.png" alt="" /> Pi357 PLC
                </a>
                <ul className="nav">
                  <li className="scroll-to-section"><a href="#top" className="active">{content.menu?.home}</a></li>
                  <li className="scroll-to-section"><a href="#services">{content.menu?.services}</a></li>
                  <li className="scroll-to-section"><a href="#portfolio">{content.menu?.portfolio}</a></li>
                  <li className="scroll-to-section"><a href="#team">{content.menu?.team}</a></li>
                  <li className="scroll-to-section">
                    <div className="border-first-button"><a href="#contact">{content.menu?.contact}</a></div>
                  </li>
                </ul>
                <a className='menu-trigger'>
                  <span>Menu</span>
                </a>
              </nav>
            </div>
          </div>
        </div>
      </header>

      <div className="main-banner wow fadeIn" id="top" data-wow-duration="1s" data-wow-delay="0.5s">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="row">
                <div className="col-lg-6 align-self-center">
                  <div className="left-content show-up header-text wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
                    <div className="row">
                      <div className="col-lg-12">
                        <h6>{content.banner?.subtitle}</h6>
                        <h2>{content.banner?.title}</h2>
                        <p>{content.banner?.text}</p>
                      </div>
                      <div className="col-lg-12">
                        <div className="border-first-button scroll-to-section">
                          <a href="#contact">{content.banner?.button}</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="right-image wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                    <img src="/assets/images/banner-image.png" alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="services" className="services section">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="section-heading  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
                <h6>{content.services?.subtitle}</h6>
                <h4 dangerouslySetInnerHTML={{ __html: content.services?.title }}></h4>
                <div className="line-dec"></div>
              </div>
            </div>
            <div className="col-lg-12">
              <div className="naccs">
                <div className="grid">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="menu">
                        <div className="first-thumb active">
                          <div className="thumb">
                            <span className="icon"><img src="assets/images/services-care.png" alt="" /></span>
                            {content.services?.service_title1}
                          </div>
                        </div>
                        <div>
                          <div className="thumb">
                            <span className="icon"><img src="assets/images/services-insurance.png" alt="" /></span>
                            {content.services?.service_title2}
                          </div>
                        </div>
                        <div>
                          <div className="thumb">
                            <span className="icon"><img src="assets/images/services-cashless.png" alt="" /></span>
                            {content.services?.service_title3}
                          </div>
                        </div>
                        <div>
                          <div className="thumb">
                            <span className="icon"><img src="assets/images/services-ads.png" alt="" /></span>
                            {content.services?.service_title4}
                          </div>
                        </div>
                        <div>
                          <div className="thumb">
                            <span className="icon"><img src="assets/images/services-fuel.png" alt="" /></span>
                            {content.services?.service_title5}
                          </div>
                        </div>
                        <div className="last-thumb">
                          <div className="thumb">
                            <span className="icon"><img src="assets/images/services-leasing.png" alt="" /></span>
                            {content.services?.service_title6}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-12">
                      <ul className="nacc">
                        <li className="active">
                          <div>
                            <div className="thumb">
                              <div className="row">
                                <div className="col-lg-6 align-self-center">
                                  <div className="left-text">
                                    <h4>&#960; {content.services?.service_title1}</h4>
                                    <p>{content.services?.service_text1}</p>
                                  </div>
                                </div>
                                <div className="col-lg-6 align-self-center">
                                  <div className="right-image">
                                    <img src="assets/images/services-image-care.png" alt="" />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div>
                            <div className="thumb">
                              <div className="row">
                                <div className="col-lg-6 align-self-center">
                                  <div className="left-text">
                                    <h4>&#960; {content.services?.service_title2}</h4>
                                    <p>{content.services?.service_text2}</p>
                                  </div>
                                </div>
                                <div className="col-lg-6 align-self-center">
                                  <div className="right-image">
                                    <img src="assets/images/services-image-insurance.jpg" alt="" />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div>
                            <div className="thumb">
                              <div className="row">
                                <div className="col-lg-6 align-self-center">
                                  <div className="left-text">
                                    <h4>&#960; {content.services?.service_title3}</h4>
                                    <p>{content.services?.service_text3}</p>
                                  </div>
                                </div>
                                <div className="col-lg-6 align-self-center">
                                  <div className="right-image">
                                    <img src="assets/images/services-image-cashless.jpg" alt="" />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div>
                            <div className="thumb">
                              <div className="row">
                                <div className="col-lg-6 align-self-center">
                                  <div className="left-text">
                                    <h4>&#960; {content.services?.service_title4}</h4>
                                    <p>{content.services?.service_text4}</p>
                                  </div>
                                </div>
                                <div className="col-lg-6 align-self-center">
                                  <div className="right-image">
                                    <img src="assets/images/services-image-ads.png" alt="" />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div>
                            <div className="thumb">
                              <div className="row">
                                <div className="col-lg-6 align-self-center">
                                  <div className="left-text">
                                    <h4>&#960; {content.services?.service_title5}</h4>
                                    <p>{content.services?.service_text5}</p>
                                  </div>
                                </div>
                                <div className="col-lg-6 align-self-center">
                                  <div className="right-image">
                                    <img src="assets/images/services-image-fuel.png" alt="" />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div>
                            <div className="thumb">
                              <div className="row">
                                <div className="col-lg-6 align-self-center">
                                  <div className="left-text">
                                    <h4>&#960; {content.services?.service_title6}</h4>
                                    <p>{content.services?.service_text6}</p>
                                  </div>
                                </div>
                                <div className="col-lg-6 align-self-center">
                                  <div className="right-image">
                                    <img src="assets/images/services-image-leasing.png" alt="" />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="portfolio" className="our-portfolio section">
        <div className="container">
          <div className="row">
            <div className="col-lg-5">
              <div className="section-heading wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                <h6>{content.portfolio?.subtitle}</h6>
                <h4 dangerouslySetInnerHTML={{ __html: content.portfolio?.title }}></h4>
                <div className="line-dec"></div>
              </div>
            </div>
          </div>
        </div>
        <div className="container-fluid wow fadeIn" data-wow-duration="1s" data-wow-delay="0.7s">
          <div className="row">
            <div className="col-lg-12">
              <div className="loop owl-carousel">
                <div className="item">
                  <a onClick={($event) => showModal($event, "taximeter")} href="#">
                    <div className="portfolio-item">
                      <div className="thumb">
                        <img src="assets/images/portfolio-pitaxi.png" alt="" />
                      </div>
                      <div className="down-content">
                        <h4>&#960; {content.portfolio?.project_title1} </h4>
                      </div>
                    </div>
                  </a>
                </div>
                <div className="item">
                  <a onClick={($event) => showModal($event, "bus")} href="#">
                    <div className="portfolio-item">
                      <div className="thumb">
                        <img src="assets/images/portfolio-pibus.png" alt="" />
                      </div>
                      <div className="down-content">
                        <h4>&#960; {content.portfolio?.project_title2} </h4>
                      </div>
                    </div>
                  </a>
                </div>
                <div className="item">
                  <a onClick={($event) => showModal($event, "cargo")} href="#">
                    <div className="portfolio-item">
                      <div className="thumb">
                        <img src="assets/images/portfolio-picargo.png" alt="" />
                      </div>
                      <div className="down-content">
                        <h4>&#960; {content.portfolio?.project_title3} </h4>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="team" className="team">
        <div className="container">
          <div className="row">
            <div className="col-lg-4 offset-lg-4  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.3s">
              <div className="section-heading">
                <h6>{content.team?.subtitle}</h6>
                <h4 dangerouslySetInnerHTML={{ __html: content.team?.title }}></h4>
                <div className="line-dec"></div>
              </div>
            </div>
            <div className="col-lg-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
              <div className="team-posts">
                <div className="row">
                  <div className="col-lg-6">
                    <div className="post-item">
                      <div className="thumb">
                        <a href="#"><img src="assets/images/team-01.png" alt="" /></a>
                      </div>
                      <div className="right-content">
                        <span className="category">{content.team?.member_job1}</span>
                        <a href="#">
                          <h4>Parfait OUATTARA</h4>
                        </a>
                        <p>parfait@pi357.com</p>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-6">
                    <div className="post-item">
                      <div className="thumb">
                        <a href="#"><img src="assets/images/team-02.png" alt="" /></a>
                      </div>
                      <div className="right-content">
                        <span className="category">{content.team?.member_job2}</span>
                        <a href="#">
                          <h4>Alain MESSOU</h4>
                        </a>
                        <p>serge@pi357.com</p>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-6">
                    <div className="post-item">
                      <div className="thumb">
                        <a href="#"><img src="assets/images/team-03.png" alt="" /></a>
                      </div>
                      <div className="right-content">
                        <span className="category">{content.team?.member_job3}</span>
                        <a href="#">
                          <h4>Ahmed SIDIBE</h4>
                        </a>
                        <p>ahmed@pi357.com</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="contact" className="contact-us section">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 offset-lg-3">
              <div className="section-heading wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <h6>{content.contact?.subtitle}</h6>
                <h4 dangerouslySetInnerHTML={{ __html: content.contact?.title }}></h4>
                <div className="line-dec"></div>
              </div>
            </div>
            <div className="col-lg-12 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.25s">
              <form id="contact" action="" method="post">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="contact-dec">
                      <img src="assets/images/contact-dec-v3.png" alt="" />
                    </div>
                  </div>
                  <div className="col-lg-5">
                    <div id="map">
                      <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7946.258206839627!2d-3.6385466763101997!3d5.24240479566175!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc21b975f115521%3A0xdf2a8dcb91dfcc30!2sYaou%20carrefour%20h%C3%B4pital!5e0!3m2!1sfr!2sci!4v1688473569220!5m2!1sfr!2sci"
                        width="100%" height="636" allowFullScreen="" loading="lazy"
                        referrerPolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                  </div>
                  <div className="col-lg-7">
                    <div className="fill-form">
                      <div className="row">
                        <div className="col-lg-4">
                          <div className="info-post">
                            <div className="icon">
                              <img src="assets/images/phone-icon.png" alt="" />
                              <a href="tel:+2250749400000">(+225) 07 49 40 00 00</a>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-4">
                          <div className="info-post">
                            <div className="icon">
                              <img src="assets/images/email-icon.png" alt="" />
                              <a href="mailto:info@pi357.com">info@pi357.com</a>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-4">
                          <div className="info-post">
                            <div className="icon">
                              <img src="assets/images/location-icon.png" alt="" />
                              <a href="https://goo.gl/maps/JGCQZnZEfUxeRxUh9">Yaou Terre Promise</a>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-6">
                          <fieldset>
                            <input type="name" name="name" id="name" placeholder={content.contact?.placeholder_name} required />
                          </fieldset>
                          <fieldset>
                            <input type="text" name="email" id="email" placeholder={content.contact?.placeholder_email} required />
                          </fieldset>
                          <fieldset>
                            <input type="text" name="phone" id="phone" placeholder={content.contact?.placeholder_phone} required />
                          </fieldset>
                        </div>
                        <div className="col-lg-6">
                          <fieldset>
                            <textarea name="message" className="form-control" id="message" placeholder={content.contact?.placeholder_message}
                              required></textarea>
                          </fieldset>
                        </div>
                        <div className="col-lg-12">
                          <fieldset>
                            <button type="submit" id="form-submit" className="main-button ">{content.contact?.button}</button>
                          </fieldset>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <footer>
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <p>{content.footer?.copyright}
              </p>
            </div>
          </div>
        </div>
      </footer>

      <dialog id="dialog">
        <div dangerouslySetInnerHTML={{ __html: projectDetails }}>

        </div>
        <button onClick={($event) => closeModal($event)} aria-label="close" className="x">❌</button>
      </dialog>
    </>
  );
}

export default App;
